import os
from zlib import adler32
import hashlib
import click
from tabulate import tabulate
from rucio.client.replicaclient import ReplicaClient
from rucio.client.ruleclient import RuleClient
from rucio.common.exception import DuplicateRule

SCRATCH_MOUNT_PATH = os.getenv('SCRATCH_MOUNT_PATH', "/scratch")
SCRATCH_PFN_PREFIX = os.getenv(
    'SCRATCH_PFN_PREFIX', "root://eoseulake.cern.ch:1094//eos/eulake/tests/jupyter-scratch")
SOURCE_RSE_NAME = os.getenv('SOURCE_RSE_NAME', "JUPYTER-SCRATCH-EULAKE")
DEFAULT_DESTINATION_RSE_EXPRESSION = os.getenv(
    'DEFAULT_DESTINATION_RSE_EXPRESSION', "EULAKE-1")
SOURCE_REPLICA_LIFETIME_DAYS = int(
    os.getenv("SOURCE_REPLICA_LIFETIME_DAYS", "2"))
RUCIO_WEBUI_URL = os.getenv("RUCIO_WEBUI_URL")


class UploadHandler:
    def __init__(self):
        self.replica_client = ReplicaClient()
        self.rule_client = RuleClient()

    def handle(self, file_paths, scope, prefix, rse_expression, lifetime_days=30, delay_deletion=False):
        if not delay_deletion:
            click.echo(
                f"Uploaded files will be deleted from {SCRATCH_MOUNT_PATH} immediately after a successful upload. Use --delay-deletion flag to delete after {SOURCE_REPLICA_LIFETIME_DAYS} days.")

        legal_paths = self.get_legal_paths(file_paths)

        lifetime_secs = lifetime_days * 24 * 60 * 60
        dids = []
        for path in legal_paths:
            replica = self.get_rucio_replica_entry(path, scope, prefix)
            dids.append(replica)

        click.echo(f"Uploading {len(dids)} files to Rucio...")
        table = []
        for did in dids:
            did_exists = self.did_exists(did['scope'], did['name'])
            if did_exists:
                click.echo(
                    f"DID {replica['scope']}:{replica['name']} exists, skipping..")
            else:
                self.create_rucio_replica(file=did)
                rule_id = self.create_rucio_replication_rule(
                    file=did, rse_expression=rse_expression, lifetime=lifetime_secs, delay_deletion=delay_deletion)

                if rule_id:
                    did = f"{did['scope']}:{did['name']}"
                    url = f"{RUCIO_WEBUI_URL}/rule?rule_id={rule_id}"
                    table.append([did, rule_id, url])
        print()
        print(tabulate(table, headers=["DID", "Rule ID", "WebUI URL"]))
        print()

        return dids

    def did_exists(self, scope, name):
        replicas = self.replica_client.list_replicas(
            dids=[{'scope': scope, 'name': name}])

        length = sum(1 for _ in replicas)
        return length > 0

    def create_rucio_replica(self, file):
        self.replica_client.add_replica(rse=SOURCE_RSE_NAME, **file)

    def create_rucio_replication_rule(self, file, rse_expression, lifetime, delay_deletion=False):
        if lifetime == 0:
            lifetime = None

        try:
            # Short-lived replication rule in the scratch RSE to allow Replica deletion
            source_replica_lifetime = 10    # 10 seconds to expire the rule immediately
            if delay_deletion:
                source_replica_lifetime = SOURCE_REPLICA_LIFETIME_DAYS * 24 * 60 * 60

            self.rule_client.add_replication_rule(
                dids=[file], copies=1, rse_expression=SOURCE_RSE_NAME, lifetime=source_replica_lifetime, ignore_availability=True)

            # Replication rule in the dest RSE
            rules = self.rule_client.add_replication_rule(
                dids=[file], copies=1, rse_expression=rse_expression, lifetime=lifetime)

            click.echo(
                f"Successfully added replication rule for {file['scope']}:{file['name']}")

            rule_id = rules[0]
            return rule_id
        except DuplicateRule:
            click.echo(
                f"Replication rule for {file['scope']}:{file['name']} exists")

    def get_rucio_replica_entry(self, path, scope, prefix):
        return dict(
            scope=scope,
            name=prefix + path.split('/')[-1],
            bytes=os.path.getsize(path),
            adler32=self.calculate_adler32(path).zfill(8),
            pfn=self.resolve_rse_pfn(path),
            md5=self.calculate_md5(path),
            meta={}
        )

    def get_legal_paths(self, paths):
        legal_paths = []
        for path in paths:
            absolute_path = os.path.abspath(path)
            if os.path.isfile(absolute_path) and absolute_path.startswith(SCRATCH_MOUNT_PATH):
                legal_paths.append(absolute_path)
        return legal_paths

    def resolve_rse_pfn(self, absolute_path):
        if not absolute_path.startswith(SCRATCH_MOUNT_PATH):
            return None

        pfn_path = absolute_path.replace(SCRATCH_MOUNT_PATH, "", 1).strip('/')
        return f"{SCRATCH_PFN_PREFIX}/{pfn_path}"

    def calculate_adler32(self, file_path):
        BLOCKSIZE = 256*1024*1024
        asum = 1
        with open(file_path, 'rb') as f:
            while (data := f.read(BLOCKSIZE)):
                # print('read len:', len(data))
                asum = adler32(data, asum)
        return format(asum, 'x')

    def calculate_md5(self, file_path):
        return hashlib.md5(open(file_path, 'rb').read()).hexdigest()
