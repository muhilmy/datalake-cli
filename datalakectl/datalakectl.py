#!/opt/conda/bin/python

import os
import click

from upload import UploadHandler, DEFAULT_DESTINATION_RSE_EXPRESSION, SCRATCH_MOUNT_PATH


@click.group()
def main():
    pass


@main.command()
@click.argument('paths', type=click.Path(exists=True), nargs=-1)
@click.option('-s', '--scope', required=True, prompt='Scope', help='Scope of the uploaded files')
@click.option('-p', '--prefix', prompt=False, default='', help='Prefix of the DID name')
@click.option('-t', '--lifetime', prompt="File lifetime (in days)", default=30, help='Replication rule lifetime in days (Use 0 for indefinite lifetime)')
@click.option('--rse', prompt=False, default=DEFAULT_DESTINATION_RSE_EXPRESSION, help='RSE expression for the replication rule')
@click.option('--delay-deletion', is_flag=True, prompt=False, default=False, help=f'If false, the file in {SCRATCH_MOUNT_PATH} will be deleted immediately after a successful upload')
def upload(paths, scope, prefix, rse, lifetime, delay_deletion):
    """Upload specified files within the scratch space to Rucio"""
    replicas = UploadHandler().handle(
        paths, scope=scope, prefix=prefix, rse_expression=rse, lifetime_days=lifetime, delay_deletion=delay_deletion)


if __name__ == "__main__":
    main()
